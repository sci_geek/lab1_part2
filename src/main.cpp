// Author:         Joel Thibault
// Net ID:         jthibault
// Date:           5 Febuwary 2017
// Assignment:     Lab 1 part 1
//
// Description: This file contains a programmatic overall description of the
// program. It should never contain assignments to special function registers
// for the exception key one-line code such as checking the state of the pin.
//
// Requirements:
//----------------------------------------------------------------------//
#include "led.h"
#include "switch.h"
#include "timer.h"
#include <Arduino.h>
#include <avr/io.h>

/*
 * Define a set of states that can be used in the state machine using an enum
 */

typedef enum state_type {
  speed1_up,
  Speed1_down,
  Speed2_up,
  Speed2_down,
  Speed1_dbUp,
  Speed1_dbDown,
  Speed2_dbUp,
  Speed2_dbDown
} state_type;

#define speed1 100
#define speed2 50
#define dbDelay 50

volatile state_type state = speed1_up;
volatile unsigned char ledChar = 1;

int main() {
  Serial.begin(9600);
  initSwitchPB3();
  initTimer1();
  initLED();
  sei(); // enable global interrupts

  /*
   * Implement a state machine in the while loop which achieves the assignment
   * requirements.
   */
  int delayCount = speed1;

  while (1) {
    /* After 100 ms, increment the binary number represented by the LEDs
     */
    delayMs(delayCount);
    // Serial.println("tick");
    // count to 15 befor overflowing
    if (ledChar < 0b1111) {
      ledChar++;
    }
    // overflow back to 0
    else {
      ledChar = 0b0000;
    }

    // turn on the leds representing the 4 bit binary
    turnOnLEDWithChar(ledChar);

    switch (state) {
    // inital state
    case speed1_up:
      // set comparitive value
      delayCount = speed1;
      break;
    // de bounce
    case Speed1_dbDown:
      Serial.println("Speed1_dbDown");
      delayMs(dbDelay);
      state = Speed1_down;
      break;
    // wait state well button is down
    case Speed1_down:
      break;
    // de bounce
    case Speed2_dbUp:
      Serial.println("Speed2_dbUp");
      delayMs(dbDelay);
      state = Speed2_up;
      break;
    // change speed
    case Speed2_up:
      // set comparitive value
      delayCount = speed2;
      break;
    // de bounce
    case Speed2_dbDown:
      Serial.println("Speed2_dbDown");
      delayMs(dbDelay);
      state = Speed2_down;
      break;
    // wait state well button is down
    case Speed2_down:
      break;
    // de bounce
    case Speed1_dbUp:
      Serial.println("Speed1_dbUp");
      delayMs(dbDelay);
      state = speed1_up;
      break;
    }
  }

  return 0;
}

/* Implement an Pin Change Interrupt which handles the switch being
 * pressed ant released. When the switch is pressed an released, the LEDs
 * change at twice the original rate. If the LEDs are already changing at twice
 * the original rate, it goes back to the original rate.
 */

// controles what the next state is with the button
ISR(PCINT0_vect) {

  switch (state) {
  case speed1_up:
    state = Speed1_dbDown;
    break;

  case Speed1_down:
    state = Speed2_dbUp;
    break;

  case Speed2_up:
    state = Speed2_dbDown;
    break;

  case Speed2_down:
    state = Speed1_dbUp;
    break;

  default:
    break;
  }
  //
  // if (state == speed1_up) {
  //   state = Speed1_dbDown;
  // }
  // else if (state == Speed1_down) {
  //   state = Speed2_dbUp;
  // }
  // else if (state == Speed2_up) {
  //   state = Speed2_dbDown;
  // }
  // else if (state == Speed2_down) {
  //   state = Speed1_dbUp;
  // }
}
