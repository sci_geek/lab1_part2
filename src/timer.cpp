// Description: This file implements functions that utilize the timers
//----------------------------------------------------------------------//

#include "timer.h"
#include <Arduino.h>

/* Initialize timer 1, you should not turn the timer on here.
* You will need to use CTC mode
*/
void initTimer1(){
  //set up timer and CTC mode
  TCCR1A &= ~(1 << WGM10 | 1<< WGM11);
  TCCR1B &= ~(1 << WGM13);
  TCCR1B |=(1 << WGM12);


  //trun off the timmer
  TCCR1B &= ~( 1<<CS10 | 1 <<CS11 | 1<<CS12 );
}

/* This delays the program an amount specified by unsigned int delay.
* Use timer 1. Keep in mind that you need to choose your prescalar wisely
* such that your timer is precise to 1 millisecond and can be used for 125
* 100 milliseconds
*/

//FIX-ME
void delayMs(unsigned int delay_s){

  TCNT1 = 0;// clear the timer
  OCR1A = 249 * delay_s;// calculate the TOP value and assign it to OCR1A
  // Turn on the timer
  //perscaler 64 and turn timer on
  TCCR1B |= ( 1<<CS10 | 1 <<CS11);
  TCCR1B &= ~(1<<CS12 );
  // Do nothing while the OCF1A flag is not up
  while (!(TIFR1 & (1 << OCF1A))) {
    //Serial.println("waiting");
  }
  //Serial.println("tick");
  TIFR1 |= (1 << OCF1A);// Clear the OCF1A flag
  // Turn off the timer
  TCCR1B &= ~( 1<<CS10 | 1 <<CS11 | 1<<CS12 );

}
